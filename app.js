var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var spawn = require('child_process').spawn;

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

app.get('/stream.ts', function(request, response) {
  setTimeout(function(){
  // Write header
  response.writeHead(200, {  'Content-Type': 'video/MP2T' });

  var ffmpeg = spawn("ffmpeg",[
                "-i","in.mkv", // if you need STDIN , set "pipe:0"
                "-map","0",
                "-f","mpegts",          // mp4
                "-vcodec","copy",
                "pipe:1" // Output -> STDOUT
  ]);
   // Pipe the video output to the client response
   ffmpeg.stdout.pipe(response);
   // Kill the subprocesses when client disconnects
   response.on("close",function(){
     ffmpeg.kill();
   })
  },1000);
});

app.get('/now.mkv', function(request, response) {
  setTimeout(function(){
  // Write header
  response.writeHead(200, {  'Content-Type': 'video/x-matroska' });

  var getmedia = spawn("python",[
                "01_getmedia3.py"
  ]);
   // Pipe the video output to the client response
   getmedia.stdout.pipe(response);
   getmedia.stderr.on('data', (data) => {
     console.log(`stderr: ${data}`);
   });
   // Kill the subprocesses when client disconnects
   response.on("close",function(){
     getmedia.kill();
   })
  },1000);
});

app.get('/now.ts', function(request, response) {
  setTimeout(function(){
    // Write header
    response.writeHead(200, {  'Content-Type': 'video/MP2T' });

    var getmedia = spawn("python",[
                  "01_getmedia3.py"
    ]);
    var ffmpeg = spawn("ffmpeg",[
                  "-f","matroska",
                  "-i","pipe:0", // if you need STDIN , set "pipe:0"
                  "-map","0",
                  "-f","mpegts",
                  "-vcodec","copy",
                  "pipe:1" // Output -> STDOUT
    ]);
    // Pipe the video output to the client response
    getmedia.stdout.pipe(ffmpeg.stdin);
    getmedia.stderr.on('data', (data) => {
      console.log(`stderr(getmedia): ${data}`);
    });

    // Pipe the video output to the client response
    ffmpeg.stdout.pipe(response);
    ffmpeg.stderr.on('data', (data) => {
      console.log(`stderr(ffmpeg): ${data}`);
    });

    // Kill the subprocesses when client disconnects
    response.on("close",function(){
      getmedia.kill();
      ffmpeg.kill();
    })
  },1000);
});

app.get('/stream.m3u8', function(request, response) {
  response.set('Content-Type', 'application/x-mpegURL');
  response.send(`
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-ALLOW-CACHE:NO
#EXT-X-TARGETDURATION:10

#EXTINF:10.0,
/stream.ts
#EXT-X-ENDLIST
`);
});

app.get('/now.m3u8', function(request, response) {
  response.set('Content-Type', 'application/x-mpegURL');
  response.send(`
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-ALLOW-CACHE:NO
#EXT-X-TARGETDURATION:10

#EXTINF:10.0,
/now.ts
#EXT-X-ENDLIST
`);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
