#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import boto3
import urllib.parse
import json
import sys
import datetime

from botocore.awsrequest import AWSRequest
from botocore.auth import SigV4Auth
from botocore.endpoint import BotocoreHTTPSession
from botocore.credentials import Credentials

from logging import getLogger, StreamHandler, DEBUG
logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)
logger.propagate = False

def RequestGetDataEndpoint(streamName, config=None):
    ### 2. Create AWSRequest
    logger.debug('Creating AWSRequest for GetDataEndPoint...')
    o = urllib.parse.urlparse(config['API_ENDPOINT'])
    url = "%s://%s%s" % (o.scheme, o.netloc, config['GET_DATA_ENDPOINT_PATH'])
    BODY_PY = {
        "AccessMode": "READ",
        "ReadType": "REALTIME",
        "StreamName": streamName
    }
    body = json.dumps(BODY_PY)
    request = AWSRequest(method="POST", url=url, data=body)

    ### 3. Add signature to request
    logger.debug('Adding signature to GetDataEndPoint...')
    SigV4Auth(config['CREDENTIALS'], config['SERVICE_NAME'], config['AWS_REGION']).add_auth(request)

    ### 4. Send request
    logger.debug('Requesting GetDataEndPoint...')
    return BotocoreHTTPSession().send(request.prepare(), stream=False)

def RequestGetMedia(endpoint, streamName, config=None):
    ### 2. Create AWSRequest
    logger.debug('Creating AWSRequest for GetMedia...')
    o = urllib.parse.urlparse(endpoint)
    url = "%s://%s%s" % (o.scheme, o.netloc, config['GET_MEDIA_PATH'])
    BODY_PY = {
        "startSelector": {
             "startSelectorType": "NOW"
#            "startSelectorType": "FRAGMENT_NUMBER",
#            "fragmentNumber" : "91343852345310282912051356041729682296770358499"
        },
        "streamName": streamName
    }
    body = json.dumps(BODY_PY)
    request = AWSRequest(method="POST", url=url, data=body)

    ### 3. Add signature to request
    logger.debug('Adding signature to GetMedia...')
    SigV4Auth(config['CREDENTIALS'], config['SERVICE_NAME'], config['AWS_REGION']).add_auth(request)

    ### 4. Send request
    logger.debug('Requesting GetMedia... url:' + url)
    return BotocoreHTTPSession().send(request.prepare(), stream=True)

def main():
    # STREAM_NAME = "bdhandap-stream"
    STREAM_NAME = "demo-stream"
    PROFILE_NAME = "acuity" # I use named profile


    ### 1. Create Credential
    # credentials = Credentials(os.environ["AWS_ACCESS_KEY_ID"], os.environ["AWS_SECRET_ACCESS_KEY"])
    session = boto3.Session(profile_name=PROFILE_NAME)
    credentials = session.get_credentials()

    config = {
        'CREDENTIALS': credentials,
        'API_ENDPOINT': "https://beta.acuity.us-west-2.amazonaws.com",
        'GET_DATA_ENDPOINT_PATH': "/getDataEndpoint",
        'GET_MEDIA_PATH': "/getMedia",
        'SERVICE_NAME': "acuity",
        'AWS_REGION': 'us-west-2'
    }

    r = RequestGetDataEndpoint(STREAM_NAME, config=config)
    if r.ok:
        data = json.loads(r.text)
        endPoint = data["DataEndpoint"]

        r = RequestGetMedia(endPoint, STREAM_NAME, config=config)
        ### 5. Read response
        # print ('\nRESPONSE++++++++++++++++++++++++++++++++++++')
        # print ('Response code: %d\n' % r.status_code)
        # print ('HEADERS++++++++++++++++++++++++++++++++++++')
        # print (r.headers)
        # print ('CONTENT++++++++++++++++++++++++++++++++++++')
        starttime = datetime.datetime.now()
        logger.debug('before time : ' + str(starttime))
        chunkSize = 0

        for chunk in r.iter_content(chunk_size=1024*1024):
            chunkSize = chunkSize + sys.getsizeof(chunk)
            sys.stdout.buffer.write(chunk)

        endtime = datetime.datetime.now()
        logger.debug('after time : ' + str(endtime))
        logger.debug('total mkv size %d' % chunkSize)
        duration = (endtime - starttime).total_seconds()
        logger.debug('duration : ' + str(duration))
        logger.debug('throughput : ' + str(chunkSize / duration))

if __name__ == '__main__':
    main()
